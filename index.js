var getDice = function(variation) {
	if (variation == 'boggle-uk') {
		// 'Easy' Boggle U.K. Edition
		return [
			['A', 'A', 'E', 'E', 'G', 'N'],
			['E', 'L', 'R', 'T', 'T', 'Y'],
			['A', 'O', 'O', 'T', 'T', 'W'],
			['A', 'B', 'B', 'J', 'O', 'O'],
			['E', 'H', 'R', 'T', 'V', 'W'],
			['C', 'I', 'M', 'O', 'T', 'U'],
			['D', 'I', 'S', 'T', 'T', 'Y'],
			['E', 'I', 'O', 'S', 'S', 'T'],
			['D', 'E', 'L', 'R', 'V', 'Y'],
			['A', 'C', 'H', 'O', 'P', 'S'],
			['H', 'I', 'M', 'N', 'Q', 'U'],
			['E', 'E', 'I', 'N', 'S', 'U'],
			['E', 'E', 'G', 'H', 'N', 'W'],
			['A', 'F', 'F', 'K', 'P', 'S'],
			['H', 'L', 'N', 'N', 'R', 'Z'],
			['D', 'E', 'I', 'L', 'R', 'X']
		];
		
	} else if (variation == 'boggle') {
		// 'Hard' Original Boggle Edition
		return [
			['A', 'A', 'C', 'I', 'O', 'T'],
			['A', 'H', 'M', 'O', 'R', 'S'],
			['E', 'G', 'K', 'L', 'U', 'Y'],
			['A', 'B', 'I', 'L', 'T', 'Y'],
			['A', 'C', 'D', 'E', 'M', 'P'],
			['E', 'G', 'I', 'N', 'T', 'V'],
			['G', 'I', 'L', 'R', 'U', 'W'],
			['E', 'L', 'P', 'S', 'T', 'U'],
			['D', 'E', 'N', 'O', 'S', 'W'],
			['A', 'C', 'E', 'L', 'R', 'S'],
			['A', 'B', 'J', 'M', 'O', 'Q'],
			['E', 'E', 'F', 'H', 'I', 'Y'],
			['E', 'H', 'I', 'N', 'P', 'S'],
			['D', 'K', 'N', 'O', 'T', 'U'],
			['A', 'D', 'E', 'N', 'V', 'Z'],
			['B', 'I', 'F', 'O', 'R', 'X']
		];
	} else {
		return [
			['A', 'A', 'C', 'I', 'O', 'T'],
			['A', 'H', 'M', 'O', 'R', 'S'],
			['E', 'G', 'E', 'L', 'U', 'Y'],
			['A', 'B', 'I', 'L', 'T', 'Y'],
			['A', 'C', 'D', 'E', 'M', 'P'],
			['E', 'G', 'I', 'N', 'T', 'V'],
			['G', 'I', 'L', 'R', 'U', 'W'],
			['E', 'L', 'P', 'S', 'T', 'U'],
			['D', 'E', 'N', 'O', 'S', 'W'],
			['A', 'C', 'E', 'L', 'R', 'S'],
			['A', 'B', 'J', 'M', 'O', 'Y'],
			['E', 'E', 'F', 'H', 'I', 'Y'],
			['E', 'H', 'I', 'N', 'P', 'S'],
			['D', 'A', 'N', 'O', 'T', 'U'],
			['A', 'D', 'E', 'N', 'V', 'Z'],
			['B', 'I', 'F', 'O', 'R', 'X']
		];
	}
};

// TODO: Load dict trie as JSON to speed up load time
var dict = new Trie();
for (var i = 0; i < words.length; ++i) {
	dict.insert(words[i]);
}

var placement = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15];

var board = $('#board');
for (var i = 0; i < 16; ++i) {
	board.append('<div></div>');
}

var scoreBoard = $('#score');

var answersDiv = $('#answers');

var shortestAnswerLength = 16;

var selected = [];

var select = function(div) {
	div.addClass('selected');
	selected.push(div);
};

//+ Jonas Raoni Soares Silva
//@ http://jsfromhell.com/array/shuffle [v1.0]
function shuffle(o) {
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
};

var lineDistance = function( point1, point2 ) {
    var xs = 0;
    var ys = 0;
     
    xs = point2.x - point1.x;
    xs = xs * xs;
     
    ys = point2.y - point1.y;
    ys = ys * ys;
     
    return Math.sqrt( xs + ys );
}

board.on('contextmenu', function() {
	return false;
});
board.on('mousedown touchstart', 'div', function(e) {
	selected = [];
	select($(this));
	return false;
});
board.on('mousemove touchmove', function(e) {
	if (selected.length > 0) {
		var div;
		{
			var target;
			if (e.touches) {
				var touch = e.touches[0];
				target = {x:touch.clientX, y:touch.clientY};
			} else {
				target = {x:e.clientX, y:e.clientY};
			}
			var boardPosition = board.position();
			target.x -= boardPosition.left;
			target.y -= boardPosition.top;
			var colSelect = Math.floor(target.x / 78);
			var rowSelect = Math.floor(target.y / 78);
			var cellCenter = {x:colSelect * 78 + 39, y:rowSelect * 78 + 39};
			if (lineDistance(cellCenter, target) > 32) {
				return;
			}
			div = $(board.children().get(rowSelect * 4 + colSelect));
		}
		if (!div.hasClass('selected')) {
			var lastSelectedIndex = board.children().index(selected[selected.length-1]);
			var index = board.children().index(div);
			var rowDiff = Math.floor(index / 4) - Math.floor(lastSelectedIndex / 4);
			var colDiff = Math.floor(index % 4) - Math.floor(lastSelectedIndex % 4);
			
			if (Math.abs(rowDiff) <= 1 && Math.abs(colDiff) <= 1) {
				select(div);
			}
		}
	}
	return false;
});
var commit = function() {
	if (selected.length > 0) {
		board.children().removeClass('selected');
		if (selected.length > 1) {
			var word = '';
			$.each(selected, function() {
				word += $(this).text().toUpperCase();
			});
			var text;
			{
				if (dict.contains(word.toLowerCase())) {
					if (word.length >= shortestAnswerLength) {
						text = word + ' ✔';
						answersDiv.children().each(function() {
							var text = $(this).text();
							if (text == word) {
								return false;
							}
							if (text[0] == '_' && text.length == word.length) {
								$(this).text(word);
								return false;
							}
						});
					} else {
						text = word;
					}
				} else {
					text = word + ' is not a word';
				}
			}
			scoreBoard.text(text);
		}
		selected = [];
	}
};
$(document).on('mouseup touchend', commit);

var start = function() {
	scoreBoard.text('');
	shuffle(placement);
	var dice = getDice();
	board.children().each(function(i){
		$(this).text(dice[placement[i]][Math.floor(Math.random() * 6)]);
	});
	
	var boardObj = new Board(4, 4);
	board.children().each(function(i){
		boardObj.charAt(Math.floor(i / 4), i % 4, $(this).text().toLowerCase());
	});
						  
	// Find and sort words by longest first
  var results = Boggle.solve(boardObj, dict).sort(function (a, b) {
    if (a.word.length == b.word.length) {
      return (a.word < b.word) ? 1 : -1;
    }
    return b.word.length - a.word.length;
  });
	
  // Remove words that do not use a specific tile
	/*var requiredTile = {row:1, col:1};
  results = results.filter(function (item, idx, arr){
    return item.tiles[requiredTile.row][requiredTile.col];
  });*/
	
	// Remove duplicates
	{
		var wordList = [];
		var oldResults = results;
		results = [];
		for (var i = 0; i < oldResults.length; ++i) {
			var result = oldResults[i];
			if (wordList.indexOf(result.word) === -1) {
				wordList.push(result.word);
				results.push(result);
			}
		};
	}
	
	shortestAnswerLength = 16;
	answersDiv.html('');
	for (var i = 0; i < 10 && i < results.length; i++) {
		var word = results[i].word;
		var div = $('<div></div>');
		div.text('________________'.substr(0, word.length));
		answersDiv.append(div);
		if (word.length < shortestAnswerLength) {
			shortestAnswerLength = word.length;
		}
				console.log(word);
	}
};

start();

$('#restart').on('click', function() {
	if (confirm('Start a new game?')) {
		start();
	}
});

$('#splash').hide();
$('#game').show();